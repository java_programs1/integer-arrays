package Array_swap;

import java.util.Arrays;

public class Swapping {
	public static void main(String[] args) {
		int[] arr= {6,4,8,2,5,1,3,7};
		
		System.out.println("Before sorting ="+Arrays.toString(arr));
		
		//for decending order
		
		for(int i=0;i<arr.length;i++)
		{
			for(int j=i+1;j<arr.length;j++)
			{
				if(arr[i]>arr[j])
				{
					int temp=arr[j];
					arr[j]=arr[i];
					arr[i]=temp;
				}
			}
		}
		
		System.out.println("Accending ="+Arrays.toString(arr));
		
		// for accending order
		
		for(int i=0;i<arr.length;i++)
		{
			for(int j=i+1;j<arr.length;j++)
			{
				if(arr[i]<arr[j])
				{
					int temp=arr[j];
					arr[j]=arr[i];
					arr[i]=temp;
				}
			}
		}
		System.out.println("Decending ="+Arrays.toString(arr));
	}

}
