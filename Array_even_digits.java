package programes;

public class Array_even_digits {
	public static void main(String[] args) {
		int[] arr = { 12, 2, 144, 5867, 13 };
		Array_even_digits obj = new Array_even_digits();

		System.out.println("even digits count is " + obj.find_even_digits(arr));
	}

	private int find_even_digits(int arr[]) {
		int result = 0;
		for (int i = 0; i < arr.length; i++) {
			if (count_digits(arr[i])) {
				result++;
			}
		}
		return result;
	}

	private boolean count_digits(int num) {
		int count = 0;
		while (num > 0) {
			num = num / 10;
			count++;
		}
		return count % 2 == 0;
	}
}
