package Problems;

public class Class_test {
	public static void main(String[] args) {
		int nums[]= {132,535,354,181,255};
		
		System.out.println(find_palindrome_number(nums));
		System.out.println(find_palindrome(nums));
		
	}

	private static int find_palindrome_number(int[] nums) {
		for(int i=0;i<nums.length;i++)
		{
			int temp=nums[i];
			int rev=0; 
			while(nums[i]>0)
			{
				int rem=nums[i]%10;  
				 nums[i]=nums[i]/10;
				 rev=(rev*10)+rem;  
			}
			if(rev==temp)
			{
				return rev;
			}
		}
		return 0;
	}

	private static boolean find_palindrome(int[] nums) {
		for(int i=0;i<nums.length;i++)
		{
			int temp=nums[i];
			int rev=0; 
			while(nums[i]>0)
			{
				int rem=nums[i]%10;  
				 nums[i]=nums[i]/10;
				 rev=(rev*10)+rem;  
			}
			if(rev==temp)
			{
				return true;
			}
		}
		return false;
	}

}
