package Problemssolving;

public class Linear_search {
	public static void main(String[] args) {
		Linear_search obj=new Linear_search();
		
		int array[]= {10,20,30,60,80,60};
		int target=60;
		
		System.out.println(obj.search(array,target));
		System.out.println(obj.small_number(array));
		System.out.println(obj.big_number(array));
	}
	private int big_number(int[] array) {
		int big=array[0];
		for(int i=0;i<array.length ;i++) {
			if(array[i]>big) {
				big=array[i];
			}
		}
		
		return big;
	}

	private int small_number(int[] array) {
		int small=array[0];
		for(int i=0;i<array.length ;i++) {
			if(array[i]<small) {
				small=array[i];
			}
		}
		
		return small;
	}

	private boolean search(int[] array, int target) {
		for(int i=0;i<array.length;i++) {
			if(array[i]==target) {
				return true;
			}
			
		}return false;	
	}
}
