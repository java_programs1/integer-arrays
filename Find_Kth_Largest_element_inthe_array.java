package String_practice;    //  Find K th Largest Element in the array

import java.util.Arrays;
import java.util.Scanner;

public class Proggram {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		
		System.out.println("enter k th value");
		int k=sc.nextInt();
		
		int[] arr= {3,2,1,2,3,1,2,4,1,4,5,5,6};
		Kth_largest_element(arr,k);
		
	}

	private static void Kth_largest_element(int[] arr,int k) {
		
		Arrays.sort(arr);  			// [1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 5, 5, 6]
		
		int uniq=1;
		for(int i=0;i<arr.length-1;i++)
		{
				if(arr[i]!=arr[i+1])
				{
					uniq++;			// find no of uniq values

				}	
		}
		
		int[] temp=new int[uniq];  	 // create an array for uniq values size
		
		int j=0;
		for(int i=0;i<arr.length-1;i++)    //note:(arr.length-1) for outbomd intex exception
		{
				if(arr[i]!=arr[i+1])
				{
					temp[j]=arr[i];   // Assign the uniq values in temp array in order
					j++;
				}	
		}
		temp[temp.length-1]=arr[arr.length-1];   // for arr last value stored to temp lat value, bcz (length-1)
									         	// [1, 2, 3, 4, 5, 6]
		
		System.out.println(k+" th largest value is "+temp[temp.length-k]);
		

	}

}
