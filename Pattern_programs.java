package programes;

import java.util.Scanner;

public class Pattern_programs {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number");
		int num=sc.nextInt();
		Pattern_programs obj=new Pattern_programs();
		
		if(num%2!=0) {
		obj.plus_pattern(num);
		
		obj.x_pattern(num);
		System.out.println();
		
		obj.star_pattern(num);
		System.out.println();
		
		obj.box_pattern(num);
		System.out.println();
		
		obj.z_pattern(num);
		}
		else {
			System.out.println("invalid entry enter odd number only");
		}
	}

	private void z_pattern(int num) {
		for(int row=0;row<num;row++) {
			for(int col=0;col<num;col++) {
				if(row==0 || row==num-1 || col+row==num-1)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
		
	}

	private void box_pattern(int num) {
		for(int row=0;row<num;row++)
		{
			for(int col=0;col<num;col++)
			{
		if(row==0 || col==0 || row==num-1 || col==num-1)
			System.out.print("* ");
		else
			System.out.print("  ");
		}
		System.out.println();
		}
	}

	private void star_pattern(int num) {
		for(int row=0;row<num;row++)
		{
			for(int col=0;col<num;col++)
			{
				if(row==col || row+col==num-1 || col==num/2 || row==num/2)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
		
	}

	private void x_pattern(int num) {
		for(int row=0;row<num;row++)
		{
			for(int col=0;col<num;col++)
			{
				if(row==col || row+col==num-1)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
		
	}

	private void plus_pattern(int num) {
		for(int row=0;row<num;row++) 
		{
			for(int col=0;col<num;col++)
			{
				if(col==num/2 || row==num/2)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
	}
		
		
	}


